function invert(obj) {
    // Returns a copy of the object where the keys have become the values and the values the keys.
    // Assume that all of the object's values will be unique and string serializable.
    // http://underscorejs.org/#invert

    let copyObj = {};

    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            copyObj[obj[key]] = key;
        }
    }
    return copyObj;
}

module.exports = { invert }