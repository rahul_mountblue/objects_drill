const pairsFunction = require('../pairs');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const result = pairsFunction.pairs(testObject);
// testing
// expOutput
//[ [ 'name', 'Bruce Wayne' ], [ 'age', 36 ], [ 'location', 'Gotham' ] ]

let valueMatched = true;
if(result[0][0] !== 'name' && result[0][0] !== 'Bruce Wayne') {
    valueMatched = false;
}

if(valueMatched ){
    console.log(result);
}