const valueFunction = require('../values');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; 

const result = valueFunction.values(testObject);
// testing
let expOutput = ['Bruce Wayne',36,'Gotham'];
let valueMatched = true;

for(let index = 0; index < expOutput.length; index++) {
    if(expOutput[index] !== result[index]) {
        valueMatched = false;
    }
}

if(valueMatched) {
    console.log(result);
}