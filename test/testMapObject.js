const mapObjectFunction = require('../mapObject');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
// callback function 

function addTenInArgument(data) {
    return data+10;
}

const result = mapObjectFunction.mapObject(testObject, addTenInArgument);
//testing
// expOutput
// age = 46 and location 'Gotham10'

let valueMatched = true;
if(result.age !== 46 && result.location !== 'Gotham10') {
    valueMatched = false;
}

if(valueMatched) {
    console.log(result);
}