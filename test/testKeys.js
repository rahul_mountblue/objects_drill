const keysFunction = require('../keys');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; 

const result = keysFunction.keys(testObject);
// testing
let expOutput = ['name','age','location'];
let valueMatched = true;

for(let index = 0; index < expOutput.length; index++) {
    if(expOutput[index] !== result[index]) {
        valueMatched = false;
    }
}

if(valueMatched) {
    console.log(result);
}