const invertFunction = require('../invert');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; 

const result = invertFunction.invert(testObject);

let valueMatched = true;
// expected output { '36': 'age', 'Bruce Wayne': 'name', Gotham: 'location' }
if(result.Gotham !== 'location') {
    valueMatched = false;
}
if(valueMatched) {
    console.log(result);
}