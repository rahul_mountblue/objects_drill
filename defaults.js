function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults

    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            // If the key's value in the object is undefined 
            // set the value as the value in the default object.

            if(obj[key] === undefined) {
                obj[key] = defaultProps[key];
            }
        }
    }

    return obj;
}

module.exports = { defaults };