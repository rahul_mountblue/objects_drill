function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs
    
    let arr = [];
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            arr.push(key);
        }
    }
    let result = arr.map((key) => [key.toString(), obj[key]]);
    return result;
}
    
module.exports = { pairs }
